﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread.Sleep(5000);

            var serverAddr = Environment.GetEnvironmentVariable("SERVER_ADDR");

            if (serverAddr == null || serverAddr.Length == 0) // sans compter l'exception possible de l'appel plus haut, on essait de valider si on est capable d'obtenir la variable
            {
                Console.WriteLine("Varible d'environnnement invalide");
                return; // quitter le programme
            }

            using (var client = new HttpClient())
            {
                //////////////////
                // Get
                var returnValue = client.GetAsync(serverAddr).Result;

                if (!returnValue.IsSuccessStatusCode)
                {
                    Console.WriteLine("Communication Not OK");
                    return;
                }
                
                Console.WriteLine("Communication OK");
                var content = returnValue.Content.ReadAsStringAsync().Result;
                var isServiceReturnedPair = JsonConvert.DeserializeObject<bool>(content);

                Console.WriteLine("Is it pair? " + isServiceReturnedPair.ToString());
            }
        }
    }
}
