using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Moq.Protected;
using Server.Controllers;
using Service.Controllers;
using System;
using System.Collections;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace TestProject
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void TestService()
        {
            var mock = new Mock<NumberController>();
            int[] array = new int[50];

            for (int i = 0; i < array.Length; i++)
            {
                var val = mock.Object.Get();
                array[i]=val;
            }

            //Console.WriteLine("[{0}]", string.Join(", ", array));

            Assert.AreEqual(array.Length, array.Distinct().Count());
        }


        [TestMethod]
        public void CheckOdd()
        {
            for (int i = 0; i <= 10; i++)
            {
                var oddInt = RandomNumberGenerator.GetInt32(0, Int32.MaxValue / 2) * 2 +1;
                Assert.AreEqual(checkParity(oddInt), false);
            }
        }

        [TestMethod]
        public void CheckEven()
        {
            for (int i = 0; i <= 10; i++)
            {
                var evenInt = RandomNumberGenerator.GetInt32(0, Int32.MaxValue / 2) * 2;
                Assert.AreEqual(checkParity(evenInt), true);
            }
        }

        private bool checkParity(int i)
        {
            var mockHttp = new Mock<HttpMessageHandler>(MockBehavior.Loose);
            mockHttp.Protected()
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                ).ReturnsAsync(new HttpResponseMessage()
                {
                    StatusCode = System.Net.HttpStatusCode.OK,
                    Content = new StringContent(i.ToString())
                });
            var mock = new Mock<ServerController>();

            mock.Protected().Setup<string>("GetServiceAddress").Returns("http://service");

            mock.Protected().Setup<HttpClient>("GetServiceClient")
                .Returns(new HttpClient(mockHttp.Object));

            return mock.Object.IsServiceGivingPair();
        }

    }

}
