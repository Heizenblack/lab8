﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Service.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NumberController : ControllerBase
    {
        // GET: /Number
        [HttpGet]
        public int Get()
        {
            int rnd = RandomNumberGenerator.GetInt32(0, Int32.MaxValue);
            return rnd;
        }
    }
}
