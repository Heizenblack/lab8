﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Server.Controllers
{
    [ApiController]
    public class ServerController : ControllerBase
    {
        protected virtual string GetServiceAddress()
        {
            return Environment.GetEnvironmentVariable("SERVICE_ADDR") + "/Number";
        }

        protected virtual HttpClient GetServiceClient()
        {
            return new HttpClient();
        }

        // GET: api/<ServerController>
        [HttpGet]
        [Route("")]
        public bool IsServiceGivingPair()
        {
            using (var client = GetServiceClient())
            {
                // Get
                var returnValue = client.GetAsync(GetServiceAddress()).Result;

                if (!returnValue.IsSuccessStatusCode)
                {
                    throw new Exception("invalid call to service");
                }

                Console.WriteLine("Communication OK - " + GetServiceAddress());
                var content = returnValue.Content.ReadAsStringAsync().Result;
                var nbReceived = JsonConvert.DeserializeObject<int>(content);
                
                return nbReceived % 2 == 0;
            }  
        }
    }
}
